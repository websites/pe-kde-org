<?php
    $page_title = "Colaboradores";
    include ( "header.inc" );
?>

<p>Aquí la lista de personas que han realizado trabajo de promoción, traducción, arte, desarrollo, difusión y participan activamente en la comunidad KDE.</p>

<ul>
    <li><a href="http://www.c.invazores.org/" title="Carlos Wertheman" target="_blank">Carlos Wertheman (invazor)</a></li>
    <li><a href="http://aucahuasi.blogspot.com/" title="Percy Triveño Aucahuasi" target="_blank">Percy Triveño Aucahuasi (aucahuasi)</a></li>
    <li><a href="https://ronnyml.wordpress.com/" title="Ronny Yabar Aizcorbe" target="_blank">Ronny Yabar Aizcorbe (ronnyml)</a></li>
</ul>

<?php
    include ( "footer.inc" );
?>
