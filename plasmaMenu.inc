<?php
  global $plasmaMenu;
  $plasmaMenu->addMenu("Inicio", "/", "blue.icon.png", "#3EABF1");
    $plasmaMenu->addMenuEntry("Acerca de KDE", "kde.php");
    $plasmaMenu->addMenuEntry("Quienes Somos", "about.php");
    $plasmaMenu->addMenuEntry("Objetivos", "goals.php");
    $plasmaMenu->addMenuEntry("Colaboradores", "contributors.php");
    $plasmaMenu->addMenuEntry("¡Únete al esfuerzo!", "join.php");

  $plasmaMenu->addMenu("Comunidad", "", "green.icon.png", "#69EE4A");
    $plasmaMenu->addMenuEntry("La Comunidad de KDE", "community.php");
    $plasmaMenu->addMenuEntry("Anuncios", "announcements.php");
    $plasmaMenu->addMenuEntry("Eventos","https://www.kde.org/events/month.php");
    $plasmaMenu->addMenuEntry("Donaciones","donate.php");
    $plasmaMenu->addMenuEntry("Código de Conducta", "code_of_conduct.php");
    $plasmaMenu->addMenuEntry("Manifiesto", "http://manifesto.kde.org/");


  $plasmaMenu->addMenu("Comunicación", "", "purple.icon.png", "#8C41F3");
    $plasmaMenu->addMenuEntry("Lista de Correo","https://mail.kde.org/pipermail/kde-latam/");
    $plasmaMenu->addMenuEntry("Foro Oficial en Español", "https://forum.kde.org/viewforum.php?f=252");
    $plasmaMenu->addMenuEntry("Planet KDE Español", "https://planetkde.org/es/");
    $plasmaMenu->addMenuEntry("Planet KDE Internacional", "https://planetkde.org/");
    $plasmaMenu->addMenuEntry("Redes Sociales", "social_networks.php");
    
  $plasmaMenu->addMenu("Aplicaciones", "", "red.icon.png", "#F13E72");
    $plasmaMenu->addMenuEntry("Desarrollo","https://www.kde.org/applications/development/");
    $plasmaMenu->addMenuEntry("Educación", "https://www.kde.org/applications/");
    $plasmaMenu->addMenuEntry("Gráficos", "https://www.kde.org/applications/graphics/");
    $plasmaMenu->addMenuEntry("Juegos", "https://www.kde.org/applications/games/");
    $plasmaMenu->addMenuEntry("Internet", "https://www.kde.org/applications/internet/");
    $plasmaMenu->addMenuEntry("Multimedia", "https://www.kde.org/applications/multimedia/");
    $plasmaMenu->addMenuEntry("Oficina", "https://www.kde.org/applications/office/");
    $plasmaMenu->addMenuEntry("Sistema", "https://www.kde.org/applications/system/");
    $plasmaMenu->addMenuEntry("Utilidades", "https://www.kde.org/applications/utilities/");
    
  $plasmaMenu->addMenu("La Familia KDE", "", "gray.icon.png", "#949494");
    $plasmaMenu->addMenuEntry("KDE","https://www.kde.org/");
    $plasmaMenu->addMenuEntry("KDE e.V", "https://ev.kde.org/");
    $plasmaMenu->addMenuEntry("KDE Argentina", "https://community.kde.org/KDE-AR");
    $plasmaMenu->addMenuEntry("KDE Brasil", "https://br.kde.org/");
    $plasmaMenu->addMenuEntry("KDE España", "http://es.kde.org/");
    $plasmaMenu->addMenuEntry("KDE India", "https://kde.in/");
    $plasmaMenu->addMenuEntry("KDE Italia", "http://www.kdeitalia.it/");
    $plasmaMenu->addMenuEntry("KDE Francia", "https://fr.kde.org/");
    $plasmaMenu->addMenuEntry("KDE Japón", "https://jp.kde.org/");
    $plasmaMenu->addMenuEntry("KDE Países Bajos", "https://kde.nl/");
    $plasmaMenu->addMenuEntry("KDE Taiwán", "http://kde.tw/");
    
?>
