<?php
    $page_title = "Código de Conducta de la comunidad KDE";
    include ( "header.inc" );
?>

<h2>Preámbulo</h2>
<p>En la comunidad de KDE, se unen participantes de todo el mundo para crear Software Libre para computadoras de escritorio. Esto es posible por el apoyo, trabajo duro y entusiasmo de miles de personas, entre ellos los que crean y usan el software de KDE.</p>

<p>Este documento ofrece una guía para asegurar que los participantes de KDE puedan cooperar eficazmente en un ambiente positivo e inspirador, y para explicar cómo juntos podemos fortalecer y apoyarnos el uno al otro.</p>

<p>Este Código de Conducta es compartido por todos los colaboradores y usuarios que se vinculan con el equipo de KDE y sus servicios a la comunidad.</p>

<h2>Visión General</h2>

<p>Este Código de Conducta presenta un resumen de los valores compartidos y el pensamiento de "sentido común" en nuestra comunidad. Los ingredientes sociales básicos que sostienen nuestro proyecto en conjunto incluyen:</p>

<ul>
    <li>Sé considerado</li>
    <li>Sé respetuoso</li>
    <li>Sé colaborativo</li>
    <li>Sé pragmático</li>
    <li>Apoya a otros en la comunidad</li>
    <li>Obtén apoyo de otros en la comunidad</li>
</ul>

<p>Nuestra comunidad se compone de varios grupos de individuos y organizaciones que más o menos se pueden dividir en dos grupos:</p>

<ul>
    <li>Colaboradores, o aquellos que añaden valor al proyecto a través de la mejora de software de KDE y sus servicios.</li>

    <li>Usuarios, o aquellos que añaden valor al proyecto a través de su apoyo como consumidores de software de KDE.</li>
</ul>

<p>Este Código de Conducta refleja las normas de comportamiento acordadas por miembros de la comunidad de KDE, en cualquier foro, lista de correo, wiki, sitio web, canal IRC, reunión pública o comunicación privada en el contexto del equipo de KDE y sus servicios. La comunidad actúa de acuerdo con las normas escritas en el presente Código de Conducta y defenderá estas normas para el beneficio de la comunidad. Los líderes de cualquier grupo, como moderadores de listas de correo, canales IRC, foros, etc., ejercerán el derecho de suspender el acceso a cualquier persona que continuamente rompa nuestro Código de Conducta compartido.</p>

<h2>Sé considerado</h2>

<p>Tus acciones y trabajo afectarán y serán utilizados por otras personas y a su vez, dependerás del trabajo y acciones de otros. Cualquier decisión que tomes afectará a otros miembros de la comunidad, y esperamos que al tomar decisiones tomes en cuenta esas consecuencias.</p>

<p>Como colaborador, asegúrate de que das todo el crédito por el trabajo de otros y ten en cuenta cómo tus cambios afectan a los demás. También se espera que trates de seguir las guías y el calendario de desarrollo.</p>

<p>Como usuario, recuerda que los colaboradores trabajan duro en su parte de KDE y se enorgullecen de ello. Si te sientes frustrado, tus problemas son más propensos a ser resueltos si puedes dar información precisa, y de forma educada, a todos los interesados.</p>

<h2>Sé respetuoso</h2>

<p>A fin de que la comunidad de KDE se mantenga en buen estado, sus miembros deben sentirse cómodos y aceptados. Tratarse unos a otros con respeto es absolutamente necesario para esto. En un desacuerdo, en primera instancia, se asume que las personas tienen buenas intenciones.</p>

<p>No toleramos ataques personales, racismo, sexismo o cualquier otra forma de discriminación. Es inevitable el desacuerdo, de vez en cuando, pero el respeto a las opiniones de los demás llevará a que ganes el respeto de tu propio punto de vista. Respetar a otras personas, su trabajo, sus contribuciones y asumida bienintencionada motivación hará que miembros de la comunidad se sientan cómodos y seguros y esto se traducirá en motivación y productividad.</p>

<p>Esperamos que los miembros de nuestra comunidad sean respetuosos cuando traten con otros colaboradores, usuarios y comunidades. Recuerda que KDE es un proyecto internacional y que puedes no ser consciente de los aspectos importantes de otras culturas.</p>

<h2>Sé colaborativo</h2>

<p>El movimiento de Software Libre depende de la colaboración: Esta ayuda a limitar la repetición de esfuerzos y mejorar la calidad del software producido. Con el fin de evitar malentendidos, trata de ser claro y conciso al solicitar o dar ayuda. Recuerde que es fácil malinterpretar correos electrónicos (sobre todo cuando no están escritos en tu idioma). Pide aclaraciones si no estás seguro de cómo se entiende algo; recuerda la primera regla - asume en primera instancia que las personas tienen buenas intenciones.</p>

<p>Como colaborador, debes tratar de colaborar con otros miembros de la comunidad, así como con otras comunidades que están interesadas ​​o dependen del trabajo que haces. Tu trabajo debe ser transparente y retroalimentado en la comunidad cuando esté disponible, no sólo cuando KDE lanza software. Si deseas trabajar en algo nuevo en  proyectos existentes, mantén informados a esos proyectos de tus ideas y progreso.</p>

<p>No siempre es posible llegar a un consenso sobre la implementación de una idea, así que no te sientas obligado de alcanzar esto antes de empezar. Sin embargo, siempre asegúrate de mantener informado de tu trabajo al mundo exterior, y publícalo de una manera que permita a desconocidos probar, discutir y contribuir a tus esfuerzos.</p>

<p>En cada proyecto, colaboradores van y vienen . Cuando sales o te desvinculas del proyecto, en su totalidad o en parte, debes hacerlo con orgullo por lo que haz logrado, y actuando responsablemente con otros que vienen después de ti a continuar el proyecto.</p>

<p>Como usuario, tu retroalimentación es importante, como lo es su forma. Comentarios mal pensados pueden causar daño y desmotivación de otros miembros de la comunidad, pero un debate alturado de problemas puede traer resultados positivos. Una palabra de aliento hace maravillas.</p>

<h2>Sé pragmático</h2>

<p>KDE es una comunidad pragmática. Valoramos resultados tangibles sobre el tener la última palabra en una discusión. Defendemos nuestros valores fundamentales como libertad y colaboración respetuosa, pero no dejamos que argumentos sobre cuestiones de menor importancia interfieran en lograr resultados más importantes. Estamos abiertos a sugerencias y bienvenidas las soluciones, independientemente de su origen. En caso de duda, apoya una solución que ayude a hacer las cosas, en vez de una que tiene cualidades teóricas, pero que no se esté trabajando. Utiliza las herramientas y métodos que ayuden a hacer el trabajo. Deja que las decisiones se tomen por los que hacen el trabajo.</p>

<h2>Apoya a otros en la comunidad</h2>

<p>Nuestra comunidad se hace fuerte por el respeto mutuo, la colaboración y el comportamiento pragmático, responsable. A veces hay situaciones en las que esto tiene que ser defendido y otros miembros de la comunidad necesitan ayuda.</p>

<p>Si eres testigo de que otros están siendo agredidos, piensa primero en cómo les puedes ofrecer apoyo personal. Si sientes que la situación está más allá de tu capacidad para ayudar de forma individual, contacta en privado a la víctima y pregúntale si es necesario alguna forma de intervención oficial. Del mismo modo, debes apoyar a cualquier persona que parezca estar a punto de agotarse, ya sea por el estrés relacionado con el trabajo o problemas personales.</p>

<p>Cuando surjan problemas, en primer lugar, considera respetuosamente, recordar a los implicados sobre nuestro Código de Conducta compartido. Los líderes se definen por sus acciones, y pueden ayudar a dar un buen ejemplo, trabajando resolviendo problemas, en el espíritu de este Código de Conducta, antes de que se extiendan.</p>

<h2>Obtén apoyo de otros en la comunidad</h2>

<p>Desacuerdos, tanto políticos como técnicos, ocurren todo el tiempo. Nuestra comunidad no es una excepción a la regla. El objetivo no es evitar desacuerdos o diferencias de opinión, sino resolverlos constructivamente. Debes recurrir a la comunidad para buscar consejos y resolver desacuerdos, y siempre que sea posible consulta al equipo más directamente implicado.</p>

<p>Piensa profundamente antes de convertir un desacuerdo en una disputa pública. Si es necesario, solicita mediación, trata de resolver diferencias por un medio lo menos altamente emocional posible. Si sientes que tu trabajo está siendo atentado, toma un tiempo para respirar, antes de escribir respuestas acaloradas. Considera la posibilidad de una prórroga de 24 horas si se utiliza un lenguaje emocional - a veces todo lo que se necesita, es un período de reflexión. Si realmente quieres actuar de una manera diferente, entonces, te animamos a publicar tus ideas y trabajo, así puede ser verificado y probado.</p>


<p><em>Este documento está licenciado bajo la licencia Creative Commons Atribución-CompartirIgual 3.0 Licencia.</em></p>

<p><em>Los autores de este documento desean agradecer a la comunidad de KDE y los que han trabajado para crear un ambiente tan dinámico para compartir y a quien ofreció sus pensamientos y sabiduría en la autoría de este documento. También nos gustaría agradecer a otras vibrantes comunidades que han ayudado a dar forma a este documento con sus propios ejemplos, como la comunidad de Ubuntu y su Código de Conducta.</em></p>

<?php
    include ( "footer.inc" );
?>