<?php
    $page_title = "Quienes Somos";
    include ( "header.inc" );
?>

<p>Somos un grupo de usuarios, colaboradores y desarrolladores de KDE en Perú que busca difundir, promover el desarrollo y ser parte de la comunidad internacional de KDE.</p>

<p>Puedes comunicarte con nosotros por estos medios:</p>

<a href="https://twitter.com/kdeperu" target="_blank">
    <img src="images/twitter-icon.png" alt="Twitter KDE Perú" title="Twitter KDE Perú" style="margin: 0; border: 0; background: 0">
</a>

<a href="https://www.facebook.com/kdeperu" target="_blank">
    <img src="images/facebook-icon.png" alt="Facebook KDE Perú" title="Facebook KDE Perú" style="margin: 0; border: 0; background: 0">
</a>

<a href="https://plus.google.com/116393418323389992556/posts" target="_blank">
    <img src="images/google-plus-icon.png" alt="Google Plus KDE Perú" title="Google Plus KDE Perú" style="margin: 0; border: 0; background: 0">
</a>
</br>

<p>
<a href="https://mail.kde.org/pipermail/kde-latam/" title="Lista de correo" target="_blank">Lista de correo</a>
</p>


<?php
    include("footer.inc");
?>