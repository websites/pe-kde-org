<?php
    $page_title = "La Comunidad de KDE";
    include ( "header.inc" );
?>

<p>La fuerza de KDE es la comunidad. Es donde empieza todo. Incluye la cultura, los valores y la misión de KDE. La comunidad es también lo que nos une y nos da una identidad. Esta identidad es KDE. Así que es natural que la marca "KDE" significa, ante todo, para la comunidad.</p>

<p>A veces usamos el término  Proyecto KDE como una forma débil para decir "comunidad de KDE". Hemos crecido mucho más allá de la situación en la que podría referirse a KDE como un "proyecto". KDE ya no es solamente un esfuerzo limitado para resolver el problema de tener un escritorio GUI (¿No debería decir entorno de escritorio?) para Linux. Se ha convertido en una próspera comunidad de personas que trabaja continuamente en la creación y mejora de software libre para usuarios finales basados en determinados valores, ideales y metas.</p>

<p>La marca de KDE transporta los valores de la comunidad de KDE, como la libertad, la excelencia técnica, la belleza, el pragmatismo, la portabilidad, los estándares abiertos, la colaboración internacional, profesionalismo, respeto y gran trabajo en equipo.</p>

<p>Es nuestra esperanza y continua ambición que KDE entregue una computación abierta, confiable, estable y libre de monopolios para ser disfrutadas desde científicos, profesionales de la computación a usuarios sin conocimientos específicos en sus quehacer diario. en el mundo entero.</p>


<?php
    include("footer.inc");
?>
