<?php
  $site_title = i18n_var("KDE Perú");
  $site_logo = "images/logo.png";
  $site_search = false;
  $site_menus = 1;
  $templatepath = "chihuahua/";

  $site_external = true;
  $site_locale = "es";
  $name = "pe.kde.org Webmaster";
  $mail = "ronny@kde.org";
  $showedit = false;

  $rss_feed_link = "/rss.php";
  $rss_feed_title = i18n_var("Últimas Noticias");

  // some global variables
  $kde_current_version = "5.3";
?>

