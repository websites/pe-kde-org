<?php
    $page_title = "Donaciones";
    $site_showkdeevdonatebutton = true;
    include ( "header.inc" );
?>

<p>Donar es la forma más fácil y rápida para apoyar eficientemente  a KDE y sus proyectos. Los proyectos de KDE están disponibles de forma gratuita, por lo tanto tu donación es necesaria para cubrir cosas que requieren dinero como servidores, encuentros de colaboradores, etc.</p>

<p><a href="https://ev.kde.org/" title="KDE e.V">KDE e.V</a> es la organización sin fines de lucro detrás de la comunidad de KDE. Su sede está en Alemania y por lo tanto las donaciones son deducibles de impuestos en Alemania. Si vives en otro país de la UE tu donación también puede ser deducible de impuestos. Por favor, consulte a tu asesor de impuestos para más detalles.</p>

<?php
    include ( "footer.inc" );
?>
