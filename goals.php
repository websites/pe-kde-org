<?php
    $page_title = "Objetivos";
    include ( "header.inc" );
?>


<ul>
<li>Promover y difundir el uso KDE como escritorio, entorno de trabajo y Software Libre.</li>
<li>Incentivar el uso, aplicación e investigación de las tecnologías de la plataforma KDE.</li>
<li>Facilitar la comunicación entre usuarios y desarrolladores de KDE.</li>
<li>Formar y mantener una comunidad sólida de KDE en Perú.</li>
<li>Establecer y mantener los medios necesarios que permitan a los peruanos colaborar con KDE.</li>
<li>Organizar eventos, charlas y talleres donde podamos mostrar el trabajo de la comunidad KDE Perú.</li>
<li>Crear un espacio de trabajo de un grupo humano relacionado con otras comunidades involucradas con KDE a nivel Internacional.</li>
<li>Participar, cooperar y trabajar en conjunto con otras comunidades de KDE en Lationamérica.</li>
</ul>

<?php
    include ( "footer.inc" );
?>
