<?php
  $page_title = "Noticias de KDE";
  include("header.inc");
?>

<?php
  print "<a href=\"rss.php\" title=\"RSS formatted news feed\" style=\"float:right\"><img src=\"images/rss.png\" width=\"28\" height=\"16\" alt=\"RSS\"/></a>";
  kde_general_news("./news.rdf", 200, false);
?>

<?php
  include("footer.inc");
?>
