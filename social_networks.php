<?php
    $page_title = "Redes Sociales";
    include ( "header.inc" );
?>

<p>Síguenos en las redes sociales:</p>

<a href="https://twitter.com/kdeperu" target="_blank">
    <img src="images/twitter-icon.png" alt="Twitter KDE Perú" title="Twitter KDE Perú" style="margin: 0; border: 0; background: 0">
</a>

<a href="https://www.facebook.com/kdeperu" target="_blank">
    <img src="images/facebook-icon.png" alt="Facebook KDE Perú" title="Facebook KDE Perú" style="margin: 0; border: 0; background: 0">
</a>

<a href="https://plus.google.com/116393418323389992556/posts" target="_blank">
    <img src="images/google-plus-icon.png" alt="Google Plus KDE Perú" title="Google Plus KDE Perú" style="margin: 0; border: 0; background: 0">
</a>
</br>


<?php
    include("footer.inc");
?>