<?php
    $page_title = "¡Únete al esfuerzo!";
    include ( "header.inc" );
?>

<p>Si deseas formar parte de la comunidad KDE Perú y participar en nuestros proyectos y/o eventos <a href="about.php"><strong>comunícate</strong></a> con nosotros por los distintos canales de comunicación.</p>

<p>Nos gustaría que nos cuentes un poco de tu experiencia con KDE, cuál puede ser tu aporte al equipo y de que parte del país provienes para así tener una mejor comunicación.</p>


<?php
    include ( "footer.inc" );
?>
