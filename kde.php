<?php
    $page_title = "Acerca de KDE";
    include ( "header.inc" );
?>

<p>KDE es un equipo internacional que coopera en el desarrollo y distribución de software libre y de código abierto para computadoras de escritorio y portátiles. Entre los  productos proporcionados por KDE, se encuentra un moderno sistema de escritorio para Linux y entornos Unix, una suite Ofimática, junto a una gran cantidad de aplicaciones orientadas a comunicación, trabajo, multimedia, educación, gráficos, entretenimiento,  aplicaciones web y desarrollo de software.</p>

<p>La Comunidad KDE se enfoca fuertemente en buscar soluciones innovadoras para problemas viejos y nuevos, creando una atmósfera vibrante y abierta para experimentar.</p>

<p>Las aplicaciones KDE están traducidas a aproximadamente 75 idiomas y están construidas con los principios de facilidad de uso y de accesibilidad moderna en mente. Las aplicaciones de KDE SC 4 funcionan de forma completamente nativa en GNU/Linux, BSD, Solaris, Windows y Mac OS X.</p>


<?php
    include("footer.inc");
?>