<?php
    $page_title = "";
    include ( "header.inc" );
?>
<span class="teaser">
    <img src="images/header.jpg" width="750" alt="KDE Perú" title="KDE Perú">
</span>

<h2 align="center">¡Bienvenido! al grupo de usuarios, colaboradores y desarrolladores de KDE en Perú.</h2>

<div align="center">
  <a href="https://twitter.com/kdeperu" target="_blank">
      <img src="images/twitter-icon.png" alt="Twitter KDE Perú" title="Twitter KDE Perú" style="margin: 0; border: 0; background: 0">
  </a>

  <a href="https://www.facebook.com/kdeperu" target="_blank">
      <img src="images/facebook-icon.png" alt="Facebook KDE Perú" title="Facebook KDE Perú" style="margin: 0; border: 0; background: 0">
  </a>

  <a href="https://plus.google.com/116393418323389992556/posts" target="_blank">
      <img src="images/google-plus-icon.png" alt="Google Plus KDE Perú" title="Google Plus KDE Perú" style="margin: 0; border: 0; background: 0">
  </a>
</div>

<?php
    kde_general_news("./news.rdf", 10, true);
    include ( "footer.inc" );
?>
