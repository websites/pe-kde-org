<?php
    $page_title = "Anuncios";
    include ( "header.inc" );
?>

<p>
Para recibir nuevos anuncios por correo electrónico, subscríbete a la <a href="https://mail.kde.org/mailman/listinfo/kde-announce"><strong>lista de correo</strong></a> de KDE Anuncios.
</p>
<p />

<!-- KF 5.12.0 -->
<strong>10 de Julio, 2015</strong> - <a href="https://www.kde.org/announcements/kde-frameworks-5.12.0.php" title="KDE lanza Frameworks 5.12.0.">Lanzado KDE Frameworks 5.12.0</a>
<br />
"<em>KDE lanza Frameworks 5.12.0.</em>"
<p />

<!-- Aplicaciones 15.04.3 -->
<strong>01 de Julio, 2015</strong> - <a href="https://www.kde.org/announcements/announce-applications-15.04.3.php" title="KDE lanza Aplicaciones 15.04.3.">Lanzado KDE Aplicaciones 15.04.3</a>
<br />
"<em>KDE lanza Aplicaciones 15.04.3.</em>"
<p />

<!-- Plasma 5.3.2 -->
<strong>30 de Junio, 2015</strong> - <a href="https://www.kde.org/announcements/plasma-5.3.2.php" title="KDE lanza Plasma 5.3.2.">Lanzado Plasma 5.3.2</a>
<br />
"<em>KDE lanza Plasma 5.3.2.</em>"
<p />

<!-- KF 5.11.0 -->
<strong>12 de Junio, 2015</strong> - <a href="https://www.kde.org/announcements/kde-frameworks-5.11.0.php" title="KDE lanza Frameworks 5.11.0.">Lanzado KDE Frameworks 5.11.0</a>
<br />
"<em>KDE lanza Frameworks 5.11.0.</em>"
<p />

<!-- Aplicaciones 15.04.2 -->
<strong>02 de Junio, 2015</strong> - <a href="https://www.kde.org/announcements/announce-applications-15.04.2.php" title="KDE lanza Aplicaciones 15.04.2.">Lanzado KDE Aplicaciones 15.04.2</a>
<br />
"<em>KDE lanza Aplicaciones 15.04.2.</em>"
<p />

<!-- Plasma 5.3.1 -->
<strong>26 de Mayo, 2015</strong> - <a href="https://www.kde.org/announcements/plasma-5.3.1.php" title="KDE lanza Plasma 5.3.1.">Lanzado Plasma 5.3.1</a>
<br />
"<em>KDE lanza Plasma 5.3.1.</em>"
<p />

<!-- Aplicaciones 15.04.1 -->
<strong>12 de Mayo, 2015</strong> - <a href="https://www.kde.org/announcements/announce-applications-15.04.1.php" title="KDE lanza Aplicaciones 15.04.1.">Lanzado KDE Aplicaciones 15.04.1</a>
<br />
"<em>KDE lanza Aplicaciones 15.04.1.</em>"
<p />

<!-- KF 5.10.0 -->
<strong>08 de Mayo, 2015</strong> - <a href="https://www.kde.org/announcements/kde-frameworks-5.10.0.php" title="Lanzado Frameworks 5.10.0.">Lanzado KDE Frameworks 5.10.0</a>
<br />
"<em>KDE lanza Frameworks 5.10.0.</em>"
<p />

<!-- Plasma 5.3 -->
<strong>28 de Abril, 2015</strong> - <a href="https://www.kde.org/announcements/plasma-5.3.0.php" title="Lanzado Plasma 5.3">Lanzado Plasma 5.3</a>
<br />
"<em>KDE lanza Plasma 5.3.</em>"
<p />

<!-- Aplicaciones 15.04 -->
<strong>15 de Abril, 2015</strong> - <a href="https://www.kde.org/announcements/announce-applications-15.04.0.php" title="">Lanzado KDE Aplicaciones 15.04 </a>
<br />
"<em>KDE lanza Aplicaciones 15.04.</em>"
<p />

<!-- Plasma 5.2.95 -->
<strong>14 de Abril, 2015</strong> - <a href="https://www.kde.org/announcements/plasma-5.2.95.php" title="">Lanzado Plasma 5.2.95</a>
<br />
"<em>KDE lanza Plasma 5.3 Beta.</em>"
<p />

<!-- KF 5.9.0 -->
<strong>10 de Abril, 2015</strong> - <a href="https://www.kde.org/announcements/kde-frameworks-5.9.0.php" title="">Lanzado KDE Frameworks 5.9.0 </a>
<br />
"<em>KDE lanza Frameworks 5.9.0.</em>"
<p />

<!-- Aplicaciones 15.04 RC -->
<strong>26 de Marzo, 2015</strong> - <a href="https://www.kde.org/announcements/announce-applications-15.04-rc.php" title="">Lanzado KDE Aplicaciones 15.04 Candidato a Lanzamiento </a>
<br />
"<em>KDE lanza Aplicaciones 15.04 Candidato a Lanzamiento.</em>"
<p />

<!-- Plasma 5.2.2 -->
<strong>24 de Marzo, 2015</strong> - <a href="https://www.kde.org/announcements/plasma-5.2.2.php" title="">Lanzado Plasma 5.2.2</a>
<br />
"<em>KDE lanza Plasma 5.2.2.</em>"
<p />

<!-- Aplicaciones 15.04 Beta 3 -->
<strong>19 de Marzo, 2015</strong> - <a href="https://www.kde.org/announcements/announce-applications-15.04-beta3.php" title="">Lanzado KDE Aplicaciones 15.04 Beta 3 </a>
<br />
"<em>KDE lanza Aplicaciones 15.04 Beta 3.</em>"
<p />

<!-- KF 5.8.0 -->
<strong>13 de Marzo, 2015</strong> - <a href="https://www.kde.org/announcements/kde-frameworks-5.8.0.php" title="">Lanzado KDE Frameworks 5.8.0 </a>
<br />
"<em>KDE lanza Frameworks 5.8.0.</em>"
<p />

<!-- Aplicaciones 15.04 Beta 2 -->
<strong>12 de Marzo, 2015</strong> - <a href="https://www.kde.org/announcements/announce-applications-15.04-beta2.php" title="">Lanzado KDE Aplicaciones 15.04 Beta 2 </a>
<br />
"<em>KDE lanza Aplicaciones 15.04 Beta 2.</em>"
<p />

<!-- Aplicaciones 15.04 Beta1 -->
<strong>6 de Marzo, 2015</strong> - <a href="https://www.kde.org/announcements/announce-applications-15.04-beta1.php" title="">Lanzado KDE Aplicaciones 15.04 Beta 1 </a>
<br />
"<em>KDE lanza Aplicaciones 15.04 Beta 1.</em>"
<p />

<!-- Aplicaciones 14.12.3 -->
<strong>3 de Marzo, 2015</strong> - <a href="https://www.kde.org/announcements/announce-applications-14.12.3.php" title="">Lanzado KDE Aplicaciones 14.12.3 </a>
<br />
"<em>KDE lanza Aplicaciones 14.12.3.</em>"
<p />

<!-- Plasma 5.2.1 -->
<strong>24 de Febrero, 2015</strong> - <a href="https://www.kde.org/announcements/plasma-5.2.1.php" title="">Lanzado Plasma 5.2.1</a>
<br />
"<em>KDE lanza Plasma 5.2.1.</em>"
<p />

<!-- KF 5.7.0 -->
<strong>14 de Febrero, 2015</strong> - <a href="https://www.kde.org/announcements/kde-frameworks-5.7.0.php" title="">Lanzado KDE Frameworks 5.7.0 </a>
<br />
"<em>KDE lanza Frameworks 5.7.0.</em>"
<p />

<!-- Aplicaciones 14.12.2 -->
<strong>3 de Febrero, 2015</strong> - <a href="https://www.kde.org/announcements/announce-applications-14.12.2.php" title="">Lanzado KDE Aplicaciones 14.12.2 </a>
<br />
"<em>KDE lanza Aplicaciones 14.12.2.</em>"
<p />

<!-- Plasma 5.2.0 -->
<strong>27 de Enero, 2015</strong> - <a href="https://www.kde.org/announcements/plasma-5.2.0.php" title="">Lanzado Plasma 5.2</a>
<br />
"<em>KDE lanza Plasma 5.2.</em>"
<p />

<!-- Plasma 5.1.95 -->
<strong>13 de Enero, 2015</strong> - <a href="https://www.kde.org/announcements/plasma-5.1.95.php" title="">Lanzado Plasma 5.1.95</a>
<br />
"<em>KDE lanzamiento Beta de Plasma 5.2.</em>"
<p />

<!-- Aplicaciones 14.12.1 -->
<strong>13 de Enero, 2015</strong> - <a href="https://www.kde.org/announcements/announce-applications-14.12.1.php" title="">Lanzado KDE Aplicaciones 14.12.1 </a>
<br />
"<em>KDE lanza Aplicaciones 14.12.1.</em>"
<p />

<!-- KF 5.6.0 -->
<strong>08 de Enero, 2015</strong> - <a href="https://www.kde.org/announcements/kde-frameworks-5.6.0.php" title="">Lanzado KDE Frameworks 5.6.0 </a>
<br />
"<em>KDE lanza Frameworks 5.6.0.</em>"
<p />
<!-- Aplicaciones 14.12 -->
<strong>17 de Diciembre, 2014</strong> - <a href="https://www.kde.org/announcements/announce-applications-14.12.0.php" title="">Lanzado KDE Aplicaciones 14.12 </a>
<br />
"<em>KDE lanza Aplicaciones 14.12.</em>"
<p />

<!-- Plasma 5.1.2 -->
<strong>16 de Diciembre, 2014</strong> - <a href="https://www.kde.org/announcements/plasma-5.1.2.php" title="">Lanzado Plasma 5.1.2</a>
<br />
"<em>KDE lanza Corrección de fallos de Plasma 5.</em>"
<p />

<!-- KF 5.5.0 -->
<strong>11 de Diciembre, 2014</strong> - <a href="https://www.kde.org/announcements/kde-frameworks-5.5.0.php" title="">Lanzado KDE Frameworks 5.5.0 </a>
<br />
"<em>KDE lanza Frameworks 5.5.0.</em>"
<p />

<?php
    include ( "footer.inc" );
?>
